var chai = require('chai');
let expect = chai.expect;
import sinon from 'sinon';
var appUtil =  require('../../app/utils/AppUtil');
var leaveApplicationService = require('../../app/services/LeaveApplicationService');

describe('Testing Tests', function () {
    it('Should always return true', (done) => {
        done();
    });

});

describe('calculateWorkingDaysService', () => {
    it('Should return 6 working days', (done) => {
        let result  = leaveApplicationService.calculateWorkingDaysService("01-01-2018", "01-07-2018", []);
        expect(result).to.equal(6);
        done();
    });

    it('Should return  working days', (done) => {
        let weeklyWorkingDays = sinon.stub(appUtil, 'weeklyWorkingDays');
        weeklyWorkingDays.withArgs([0,6]).returns([2,3,4,5]);
        let result  = leaveApplicationService.calculateWorkingDaysService("01-01-2018", "01-07-2018", [0,6]);
        expect(result).to.equal(4);
        done();
    });
});