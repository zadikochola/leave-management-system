import { LOGIN, LOGOUT } from './type';

export const logoutUser = () => dispatch => {
    fetch('http://localhost:3001/api/login')
        .then(data => data.json())
        .then(data => dispatch({
            type: LOGOUT,
            payload: data
        }));
}
