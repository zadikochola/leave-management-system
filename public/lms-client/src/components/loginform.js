import React,{Component} from 'react';
import { connect } from "react-redux";
import { logoutUser } from '../action/loginaction'
class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state={
      email:'',
      password:''
    }
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onSubmit(e){
    e.preventDefault();
    this.props.logoutUser();
  }
  onChange(e){
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
  }
  render(){
    return(
      <div className='inner-sm'>
      <form className="login-form" onSubmit={this.onSubmit}>
      <div className='card'>
      <div align="center" style={{backgroundColor: '#6ca439', color: 'cornsilk' }} className="card-header">
      <h3 className="title">iProcure LMS</h3>
      </div>
      <div className="card-body">
      <div className="form-group">
      <label className="sr-only" htmlFor="form-username">Email</label>
      <input type="text" id='form-username'onChange={this.onChange} name="email" required placeholder="Email..." className="form-username form-control"/>
      </div>
      <div className="form-group">
      <label className="sr-only" htmlFor="form-password">Password</label>
      <input type="password" required onChange={this.onChange} name="password" placeholder="Password..." className="form-password form-control" id="password"/>
      </div>
      <button type="submit" className="btn  btn-success btn-block">Login</button><br/>
      <div align="center">
      <p>
      <a  href="#" role="button" aria-expanded="false">
      <b>Forgot password?</b>
      </a>
      </p>
      </div>
      <div align="center" style={{backgroundColor: 'black', color: 'white' }} className="card-footer">
      <b>Don't have an account yet?</b><br/>
      <a className="btn btn-light btn-sm btn-block" href="/signup" role="button" aria-expanded="false">
      register
      </a>
      </div>
      </div>
      </div>
      </form>
      </div>
    )
  }
}
export default connect(null, { logoutUser })(LoginForm)
