import React, { Component} from 'react';
import { Provider } from 'react-redux';
import MainRouter from './router/mainrouter';
import 'bootstrap/dist/css/bootstrap.css';
import './css/main.css';
import store from './store/mainstore';
class App extends Component {
  render() {
    return (
      <Provider store={store}>
      <MainRouter />
      </Provider>
    );
  }
}

export default App;
