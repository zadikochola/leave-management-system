import { LOGIN, LOGOUT } from '../action/type';

const initialstate = {
    userinfo: [],

}

export default function (state = initialstate, action) {
    switch (action.type) {
        case LOGOUT:
            return {
                ...state, userinfo: action.payload
            };

        default:
            return state;

    }
}
