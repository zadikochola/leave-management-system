import React, { Fragment } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import LoginForm from '../components/loginform';
const MainRouter = () => {
  return (
    <Router>
    <div className='container-fluid'>
    <Route exact path="/" component={LoginForm} />

    </div>
    </Router>
  )
}
export default MainRouter
