import passport from "passport";
import localStrategy from "passport-local";
import User from '../app/models/User';
const passportJWT = require('passport-jwt');
const JWTStrategy = passportJWT.Strategy;
const ExtractJwt = passportJWT.ExtractJwt;
import config from './config-secret'; // get our config-secret file

const LocalStrategy = localStrategy.Strategy;
const USER_STATUS_PENDING = 0;
const USER_STATUS_ACTIVE = 1;
const USER_STATUS_VOIDED = 2;

export default () => {
    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });

    passport.use("local-login", new LocalStrategy({
            usernameField: 'Email',
            passwordField: 'Password',
            passReqToCallback: true
        }, (req, email, password, done) => {
        req.checkBody('Email', 'Enter valid email').notEmpty().isEmail();
        req.checkBody('Password', 'Password must be 4-15 characters').notEmpty().len(4, 15);
        var errors = req.validationErrors();
        if (errors) {
            return done(null, false, errors);
        }

        User.findOne({ 'Email': email }, (err, user) => {
            if (err) {
                return done(null, false, [{ msg: err}]);
            }

            if (!user) {
                return done(null, false, [{ msg: 'User with such email does not exist'}]);
            }
            if (!user.validPassword(password)) {
                return done(null, false, [{ msg: 'Password incorrect.'}]);
            }
            if (user.AccountStatus == USER_STATUS_PENDING) {

                return done(null, false, [{ msg: 'You need to confirm your email.'}]);
            }
            if (user.AccountStatus == USER_STATUS_VOIDED) {
                return done(null, false, [{ msg: 'Your account has been deactivated. Please contact your HR for reactivation.'}]);
            }
            return done(null, user);
        });
    }));

    passport.use(new JWTStrategy({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: config.secret,
            passReqToCallback: true
        },
        (req, jwtPayload, done) => {

            if (jwtPayload.expires < Date.now()) {
                return done([{ msg: 'jwt expired'}]);
            }
            User.findOne({ "Email": jwtPayload.email }, (err, user) => {
                if (err) {
                    return done([err]);
                }
                if (user === null) {
                    return done(null, false, [{ msg: "The user in the token was not found" }]);
                }
                return done(null, { _id: user._id, email: user.Email });
            });
        }
    ));

};