/**
 * Created by Yunus on 11/26/2018.
 */
var acl = require('acl');
var mongoose = require('mongoose');

acl = new acl(new acl.mongodbBackend(mongoose.connection.db, "acl_"), { debug: function(string) { console.log(string); } });

module.exports = {
    init: function() {
        // Do some initialization here
        var userId = "5bd1cf07a3dd1752005374a4";

        acl.allow([
            {
                roles: ['hr'],
                allows: [
                    {
                        resources: '/users',
                        permissions: 'get'
                    },
                    {
                        resources: '/departments',
                        permissions: 'get'
                    }
                ]
            },
            {
                roles: ['supervisor'],
                allows: [
                    {
                        resources: '/leave-applications',
                        permissions: 'get'
                    }
                ]
            }
        ]);

        acl.addUserRoles(userId, ['hr'])
    },

    getAcl: function() {
        return acl;
    }
};