import swaggerUi from "swagger-ui-express";
import swaggerJsdoc from 'swagger-jsdoc';

export default (app) => {
    const options = {
        swaggerDefinition: {
            // Like the one described here: https://swagger.io/specification/#infoObject
            info: {
                "title": "iProcure Leave Management Application",
                "description": "System to be used by iProcure employees to help application and management of employees leaves.",
                "termsOfService": "https://iprocu.re/lms/terms/",
                "contact": {
                    "name": "API Support",
                    "url": "https://iprocu.re/lms/support",
                    "email": "support@iprocu.re"
                },
                "license": {
                    "name": "Apache 2.0",
                    "url": "https://www.apache.org/licenses/LICENSE-2.0.html"
                },
                "version": "1.0.0"
            },
        },
        // List of files to be processes. You can also set globs './app/routes/*.js'
        apis: ['./app/routes/definitions.js', './app/routes/api/*.js'],
    };

    const specs = swaggerJsdoc(options);
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));
}