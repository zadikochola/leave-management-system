/**
 * Created by Yunus on 6/26/2018.
 */

import {findAllLeaveTypesService} from '../services/LeaveTypeService';
import {createLeaveApplicationService, calculateWorkingDaysService, getLeaveApplicationService} from '../services/LeaveApplicationService';
import LeaveApplication from '../models/LeaveApplication';
import {generateErrorsArray} from '../utils/ErrorsUtil';
import {findEmployeeById} from '../services/UserService';

export const getLeaveApplications = async (req, res, next) => {
    var leaveTypes = await findAllLeaveTypesService();
    getLeaveApplicationService()
        .then((leaveApplications)=>{
            var leaveApplicationsWithDuration = [];
            for(let leaveApplication of leaveApplications){
                leaveApplication = { ...leaveApplication.toObject(), duration : leaveApplication.workingDays()};
                leaveApplicationsWithDuration.push(leaveApplication);
            }
            res.render("leave-application/leave-applications", {leaveTypes: leaveTypes, leaveApplications : leaveApplicationsWithDuration});
        })
        .catch((errors)=>{
            console.log(errors)
        });
}

export const createLeaveApplication = (req, res, next) => {
    var leaveData = req.body;
    var leaveAppication = new LeaveApplication();
    leaveAppication.start_date = leaveData.startDate;
    leaveAppication.end_date = leaveData.endDate;
    leaveAppication.employee = leaveData.employee;
    leaveAppication.leave_type = leaveData.leaveType;
    createLeaveApplicationService(leaveAppication)
        .then((nLeaveApplication)=>{
            res.json(nLeaveApplication);
        })
        .catch((errors)=>{
            console.log(errors);
            let errorsArray = generateErrorsArray(errors.errors);
            res.status(500).send(errorsArray);
        });

}

export const getWorkingDays = (req, res, next) => {
    var employeeId = req.params['employeeId'];
    var startDate = req.body.startDate;
    var endDate = req.body.endDate;

    console.log('startDate ',startDate)
    console.log('endDate ',endDate)

    findEmployeeById(employeeId)
        .then((user)=>{
          let workingDays=calculateWorkingDaysService(startDate, endDate, user.DaysOffDuty);
            res.json(workingDays);
        })
        .catch((errors)=>{
            console.log(errors);
            next(errors);
        })
}
