import User from "../models/User";
import { findAllUserService, registerUserService, upDateUserService,deleteUserService,updateUserStatusService } from '../services/UserService';
import {findAllDepartmentService} from "../services/DepartmentService";
import {findAllLeaveTypesService} from "../services/LeaveTypeService";
import {sendEmail} from "../utils/SendEmailUtil";
import UnauthorizedAccessError from '../errors/UnauthorizedAccessError';
import BadRequestError from '../errors/BadRequestError';
import passport from "passport";
import jwt from 'jsonwebtoken';
import config from '../../config/config-secret'; // get our config-secret file

export const login = (req, res, next) => {
    passport.authenticate('local-login', {session: false}, (error, user, info) => {
            if (error || !user) {
                return next(new BadRequestError("400", info))
            }

            /** This is what ends up in our JWT */
            const payload = {
                email: user.Email,
                expires: Date.now() + parseInt(config.expiresIn),
            };

            /** assigns payload to req.user */
            req.login(payload, {session: false}, (error) => {
                if (error) {
                    //return res.status(400).send({ error });
                    return next(new BadRequestError("400", info))
                }

                /** generate a signed json web token and return it in the response */
                const token = jwt.sign(JSON.stringify(payload), config.secret);

                /** assign our jwt to the cookie */
                res.cookie('jwt', jwt, { httpOnly: true, secure: true });
                res.status(200).send({ "username":payload.username, token });
            });
        },
    )(req, res);
}

export const authenticateRequest = (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (error, user, info) => {
        if(error || !user)
            next(new UnauthorizedAccessError("403", [info]));
        req.logIn(user, function(err) {
            if (err) { return next(err); }
            next();
        });

    })(req, res, next);
}

export const getAllUsers = async(req,res,next)=>{

    const departments = await getAllDepartments();
    const leaveTypes = await findAllLeaveTypesService();

    findAllUserService()
        .then(users=>{
          let data={
            userslist: users,
            leaveTypes: leaveTypes,
            departmentlist:departments
          };
          res.status(200).json(data);

        }).catch(errors=>{
            next(errors);
        });
}


export const logout = (req, res) => {
    //des token then respond
    let data={
      success:1
    }
    res.json(data);


}

export const isUserLoggedIn = (req, res, next) => {
    //check if user is logged in i.e check if the jwt token sent in the header is validator
    let tokenvalidity=true;
    if(tokenvalidity){
        next();
    }else{
        //token not validator
      let data={
        success:0,
        message:"user loged out"
      }
      res.json(data);
    }

}

export const isAuthorized = (req, res, next) => {
    var resource = req.route.path;
    var action = req.method;
    var userId = req.user._id;
    console.log('Loggedin user: '+userId)
    console.log('Resource: '+resource)
    console.log('Action performed: '+action)

    var acl = require('../../authorization').getAcl();

    acl.isAllowed(userId, resource, action.toLocaleLowerCase(), function (err, isAllowed) {
        if (err) {
            console.log('isAuthorized error')
            next(new Error('Unexpected authorization error', 500));
        }
        console.log(isAllowed);
        if(isAllowed){
            console.log('isAuthorized success')
            next();
        }else{
            next(new UnauthorizedAccessError("403", [{msg: 'Request not allowed'}]));
        }
    });

}

export const register = (req, res, next) => {
    req.checkBody('Fname', 'Enter valid first name').notEmpty();
    req.checkBody('Lname', 'Enter valid last name').notEmpty();
    req.checkBody('Phone', 'Enter valid Phone').notEmpty();
    req.checkBody('Email', 'Enter valid email').notEmpty().isEmail();
    req.checkBody('Password', 'Password must be 4-15 characters').notEmpty().len(4, 15);
    req.checkBody('confirmPassword', 'Confirm password do not match password').notEmpty().equals(req.body.Password);
    var errors = req.validationErrors();

    if (errors) {
        next(new BadRequestError("400", errors))
    }
    else {
        console.log("registerUserService ", req.body);
        registerUserService(req.body)
            .then(user => {
                sendEmail(user,req);
                res.status(201).json({email:user.Email});
            }).catch(errors => {
                let errorsArray = generateErrorsArray(errors.errors);
                console.log("registerUserService: "+errorsArray);
                next(new BadRequestError("400", errorsArray))
            })
    }

    const generateErrorsArray = (errorObject) => {
        let errorsArray = []
        for (let key in errorObject) {
            errorsArray.push({ msg: errorObject[key].message })
        }
        return errorsArray
    }
}

const getAllDepartments = async()=>{
    try {
       const departmentlist = await findAllDepartmentService();
       return departmentlist;
    } catch (error) {
        next(error)
    }
}

export const deleteuser =(req,res,next)=>{
        deleteUserService(req.body);
        if(req.session.user._id === req.body._id)
            req.session.destroy();
            let data={
              success:1,
            };

        res.json(data);
}
export const updateUser = (req, res, next)=>{
    let user = new User(req.body);
    user.DaysOffDuty = convertStringArrayToIntArray(user.DaysOffDuty);
    upDateUserService(user);
    let data={
      success:1
    }
    res.json(data);
}

export const confirmEmail =(req,res,next)=>{
    updateUserStatusService(req.params.id);
    res.redirect("/");
}

var convertStringArrayToIntArray = function(stringArray) {
    let mselectedArray = [];
    stringArray.forEach(value => {
        let parsed = parseInt(value, 10);
        mselectedArray.push(parsed);
    });
    return mselectedArray;
}
