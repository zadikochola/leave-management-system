import {findCurrentLeaveTypeStatusService, findAllLeaveTypesService, saveNewLeaveTypeService, createEmployeeLeaveTypesService, findEmployeeLeaveTypeService} from '../services/LeaveTypeService';
import {findAllUserService} from '../services/UserService';
import LeaveType from '../models/LeaveType';
import {generateErrorsArray} from '../utils/ErrorsUtil';

export const getCurrentLeaveTypeStatus = (req, res, next) => {
    findCurrentLeaveTypeStatusService(req.params.employeeId, req.params.leaveTypeId)
        .then(employeeLeaveType => {
            res.json(employeeLeaveType);
        })
        .catch(errors => {
            next(errors);
        })
}

export const createNewLeaveType = (req, res, next) => {
    var leaveType = new LeaveType;
    leaveType.name = req.body.name;
    leaveType.AddedBy = req.session.user._id;

    saveNewLeaveTypeService(leaveType)
        .then(nLeaveType => {
            res.json(nLeaveType);
        })
        .catch(errors => {
            let errorsArray = generateErrorsArray(errors.errors);
            res.status(500).send(errorsArray);
        });
}

export const getLeaveTypes = async (req, res, next) => {
    const employees = await findAllUserService();
    findAllLeaveTypesService()
        .then(leaveTypes => {
          let data={
            success:1,
            leaveTypes: leaveTypes,
            employees: employees
          }
            res.json(data);
        })
        .catch(errors => {

        })
}

export const createEmployeeLeaveTypes = (req, res, next) => {
    createEmployeeLeaveTypesService(req.params.employeeId, req.body, req.session.user._id)
        .then((value) => {
            console.log('value ', value);
            res.json(value)
        })
        .catch((errors)=>{
            console.log('errors ', errors);
            res.status(500).json(value);
        });
}

export const getCurrentLeaveTypes = (req, res, next) => {
    findEmployeeLeaveTypeService(req.params.employeeId)
        .then((employeeLeaveTypes)=>{
            res.json(employeeLeaveTypes);
        })
        .catch((errors)=>{
            res.status(500).json(errors);
        });
}
