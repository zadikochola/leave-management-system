import mongoose from "mongoose"
import { findAllDepartmentService, addNewDepartmentService, updateDepartmentService, deleteDepartmentService } from '../services/DepartmentService'
import { findAllUserService } from '../services/UserService';
import Department from "../models/Department";
import {generateErrorsArray} from '../utils/ErrorsUtil';

export const addDepartment = (req, res, next) => {

    req.checkBody('Name', 'Enter valid Department name').notEmpty().len(3, 30);
    var errors = req.validationErrors();

    if (errors) {
        req.flash("formBody", req.body);
        req.flash("errors", errors);
        let data={
          errors:errors,
          success:0
        }
        res.json(data);;
    }
    else {
        let department = new Department();
        department.Name = req.body.Name;
        department.AddedBy = req.session.user._id;
        if (mongoose.Types.ObjectId.isValid(req.body.DepartmentHead)) {
            department.DepartmentHead = req.body.DepartmentHead;
        }
        addNewDepartmentService(department)
            .then(department => {
                res.redirect("/departments")
            }).catch(errors => {
                req.flash("formBody", req.body);
                console.log(errors);
                let errorsArray = generateErrorsArray(errors.errors);
                req.flash("errors", errorsArray);
                let data={
                  success:0,
                  errors:errorsArray
                }
                res.send(data);
            });
    }
}

export const deletedepartment = (req, res, next) => {
    deleteDepartmentService(req.body);
    let data={
      success:0
    }
    res.json(data);
}

export const updateDepartment = (req, res, next) => {
    updateDepartmentService(req.body).then(dept =>{
      let data={
        success:0,
        department:dept
      }
        res.json(data);

    }).catch(dept => {
        let errorsArray= [{ msg: "You can not use the name " + dept.Name.toUpperCase() + " because the name is already taken by another department" }];
        let data={
          success:0,
          errors:errorsArray
        }
        res.json(data);
    });
}

export const getDepartments = async (req, res) => {
    let departmentlist = await getAllDepartments();

    findAllUserService()
        .then(users => {
          let data= {
              success:1,
              userslist: users,
              departmentlist: departmentlist
          }
            res.json(data);
        }).catch(errors => {
            let data={
                success:0,
                userslist: null,
                departmentlist: departmentlist
            };
            res.json(data);
        });
}

export const getAllDepartments = async () => {
    try {
        const departmentlist = await findAllDepartmentService();
        return departmentlist;
    } catch (error) {
        next(error)
    }
}
