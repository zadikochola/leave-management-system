

export const generateErrorsArray = (errorObject) => {
    let errorsArray = []
    for (let key in errorObject) {
        errorsArray.push({ msg: errorObject[key].message })
    }
    return errorsArray
}