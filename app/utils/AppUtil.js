
export const weeklyWorkingDays = (weeklyOffDaysArray) => {
    return [0, 1, 2, 3, 4, 5, 6].filter(e => !weeklyOffDaysArray.includes(e));
}