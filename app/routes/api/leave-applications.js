import * as leaveApplicationController from "../../controllers/LeaveApplicationController";
import * as userController from "../../controllers/UserController";

var router = require('express').Router();

/**
 * @swagger
 * /api/leave-applications/working-days/:employeeId:
 *   post:
 *     tags:
 *       - Leave Applications
 *     description: Fetch employee working days count
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Working days count
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.post("/working-days/:employeeId", userController.isUserLoggedIn, leaveApplicationController.getWorkingDays)

/**
 * @swagger
 * /api/leave-applications:
 *   get:
 *     tags:
 *       - Leave Applications
 *     description: Returns all leave applications
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of leave applications
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.get("/", userController.isUserLoggedIn, leaveApplicationController.getLeaveApplications)

/**
 * @swagger
 * /api/leave-applications:
 *   post:
 *     tags:
 *       - Leave Applications
 *     description: Creates a new leave application
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Created leave application object
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.post("/", userController.isUserLoggedIn, leaveApplicationController.createLeaveApplication)

module.exports = router;