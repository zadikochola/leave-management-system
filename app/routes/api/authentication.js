import * as userController from "../../controllers/UserController";
import passport from "passport";
var router = require('express').Router();

router.post("/signup", userController.register)

router.post("/login", userController.login)

router.get("/logout", userController.logout)

router.get("/emailconfirmation/:id", userController.confirmEmail)

module.exports = router;
