import * as departmentController from "../../controllers/DepartmentsController"
import * as userController from "../../controllers/UserController";

var router = require('express').Router();

/**
 * @swagger
 * /api/departments:
 *   get:
 *     tags:
 *       - Departments
 *     description: Returns all departments
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of departments
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.get("/", userController.isUserLoggedIn, departmentController.getDepartments)

/**
 * @swagger
 * /api/departments:
 *   post:
 *     tags:
 *       - Departments
 *     description: Create a department
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Created department object
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.post("/", userController.isUserLoggedIn, departmentController.addDepartment)

/**
 * @swagger
 * /api/departments:
 *   put:
 *     tags:
 *       - Departments
 *     description: Updates a department
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Updated department object
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.put("/", userController.isUserLoggedIn, departmentController.updateDepartment)

/**
 * @swagger
 * /api/departments:
 *   get:
 *     tags:
 *       - Departments
 *     description: Delete a department
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Empty object
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.delete("/", userController.isUserLoggedIn, departmentController.deletedepartment)

module.exports = router;