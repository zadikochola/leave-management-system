var router = require('express').Router();
import * as userController from "../../controllers/UserController";
import passport from "passport";

/**
 * @swagger
 * /api/users:
 *   get:
 *     tags:
 *       - Users
 *     description: Returns all users
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of users
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.get("/", userController.authenticateRequest, userController.getAllUsers);

router.get("/test", userController.authenticateRequest, (req, res, next)=>{
    console.log('success test ')
    return res.status(200).send({user:"uyunu"})
})

/**
 * @swagger
 * /api/users:
 *   update:
 *     tags:
 *       - Users
 *     description: Update a user
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Updated user
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.put("/", passport.authenticate('jwt', { session : false }), userController.updateUser);

/**
 * @swagger
 * /api/users:
 *   delete:
 *     tags:
 *       - Users
 *     description: Delete a user
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Empty object
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.delete("/", passport.authenticate('jwt', { session : false }), userController.deleteuser);

module.exports = router;