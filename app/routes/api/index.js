var router = require('express').Router();

router.use('/users', require('./users'));
router.use('/leave-applications', require('./leave-applications'));
router.use('/departments', require('./departments'));
router.use('/leave-types', require('./leave-types'));
router.use('/', require('./authentication'));


module.exports = router;