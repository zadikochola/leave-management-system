import * as leaveTypeController from "../../controllers/LeaveTypeController";
import * as userController from "../../controllers/UserController";
var router = require('express').Router();

/**
 * @swagger
 * /api/leave-types:
 *   get:
 *     tags:
 *       - Leave Types
 *     description: Returns all leave types
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of users
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.get("/", userController.isUserLoggedIn, leaveTypeController.getLeaveTypes)

/**
 * @swagger
 * /api/leave-types:
 *   post:
 *     tags:
 *       - Leave Types
 *     description: Creates a leave type
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of users
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.post("/", userController.isUserLoggedIn, leaveTypeController.createNewLeaveType)

/**
 * @swagger
 * /api/leave-types/employee-leaves/:employeeId:
 *   post:
 *     tags:
 *       - Leave Types
 *     description: Creates a leave type
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of users
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.get("/employee-leaves/:employeeId", userController.isUserLoggedIn, leaveTypeController.getCurrentLeaveTypes)

/**
 * @swagger
 * /api/leave-types/employee-leaves/:employeeId:
 *   post:
 *     tags:
 *       - Leave Types
 *     description: Creates a employee leave type
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of users
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.post("/leave-types/employee-leaves/:employeeId", userController.isUserLoggedIn, leaveTypeController.createEmployeeLeaveTypes)

/**
 * @swagger
 * /api/leave-types/:leaveTypeId/:employeeId:
 *   get:
 *     tags:
 *       - Leave Types
 *     description: Get employee leave type status
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Leave type object
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.get("/leave-types/:leaveTypeId/:employeeId", userController.isUserLoggedIn, leaveTypeController.getCurrentLeaveTypeStatus)

module.exports = router;