import User from '../models/User';

export const findAllUserService =()=>{
    return  User.find({AccountStatus:{$ne:2}}).select('-Password').populate("Department").populate("Supervisor", "-Password").exec();
}
export const findUserByEmailService = (email) => {
    return  User.findOne({"Email" : email}).exec();
}

export const registerUserService = (userInfo)=>{
    let user = new User(userInfo)
    user.Password = user.generateHash(user.Password);
    return user.save();
}

export const upDateUserService = (user)=>{
    console.log(user);
    return User.findOneAndUpdate({Email: user.Email},{
        $set:{
            Fname:user.Fname,
            Lname:user.Lname,
            StaffNo:user.StaffNo,
            Phone:user.Phone,
            UserLevel:user.UserLevel,
            Department:user.Department,
            Supervisor:user.Supervisor,
            DaysOffDuty:user.DaysOffDuty,
            isHR:user.isHR
        }
    } ,{ upsert: true},function(err, user) {
        if (err)
           console.log(err);
        return true;
    }); 
}

export const deleteUserService =(userId)=>{
    console.log(userId);
    return User.findByIdAndUpdate({_id:userId._id},{
        $set:{
            AccountStatus:2
        }
    },{upsert:false},function(err,user){
        if(err)
            console.log(err);
            console.log("user updated success fully",user);
        return true;
    });
}

export const updateUserStatusService = (id)=>{
    console.log(id);
    return User.findOneAndUpdate({_id:id},{
        $set:{AccountStatus:1}
    },
        { upsert: false },function(err, user) {
        if (err)
           console.log(err);
        return true;
    }); 
}
export const findEmployeeById = (employeeId) => {
    return User.findById(employeeId).select('-Password').exec();
}
