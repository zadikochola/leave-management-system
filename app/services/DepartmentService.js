import Department from '../models/Department';
import mongoose from "mongoose";


/**
 * Returns a list of departments
 * @return {Promise} Promise object represents list of departments
 * */
export const findAllDepartmentService = () => {
    return Department.find().populate("DepartmentHead").populate("AddedBy").exec();
}

/**
 * Returns just saved department
 * @return {Object} Promise object represents a department
 * */
export const addNewDepartmentService = (department)=>{
    return department.save();
}

export const updateDepartmentService = (dept)=>{
   
    return new Promise((resolve, reject) => {
    Department.findByIdAndUpdate({_id:dept.departmentId},
        { $set:{
            Name:dept.Name,
            DepartmentHead:dept.DepartmentHead
        }},
        { upsert: true,new:true},
        function(err, savedDept) {
        if (err){
            reject(dept);
        }else{
            resolve(savedDept);
        }
    });  
    });
}

export const deleteDepartmentService=(deptId)=>{
    return Department.findByIdAndRemove(deptId._id,(err,dept)=>{
        if(err)
            console.log(err);
        console.log("deleted dept",dept);
        return true;
    });
}
