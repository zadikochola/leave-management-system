import LeaveApplicationLog from '../models/LeaveApplicationLog';

export const findLeaveApplicationLogs = () => {
    return LeaveApplicationLog.find();
}

export const createLeaveApplicationLog = (leaveApplicationLog) => {
    return leaveApplicationLog.save();
}