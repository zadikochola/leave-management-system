import momentBusiness from 'moment-business-days';
import {weeklyWorkingDays} from '../utils/AppUtil';
import LeaveApplication from '../models/LeaveApplication';
import LeaveApplicationLog from '../models/LeaveApplicationLog';

export const createLeaveApplicationService = (leaveApplication) => {
    leaveApplication.resolved_status = 1; //Draft

    return new Promise((resolve, reject) => {
        leaveApplication.save()
            .then(nLeaveApplication => { return nLeaveApplication; })
            .then(async (nLeaveApplication) => {
                let leaveApplicationLog = new LeaveApplicationLog();
                leaveApplicationLog.employee = leaveApplication.employee;
                leaveApplicationLog.resolved_status = leaveApplication.resolved_status;
                leaveApplicationLog.resolved_by = leaveApplication.resolved_date;
                leaveApplicationLog.leave_application = nLeaveApplication._id;
                 await leaveApplicationLog.save();
                return nLeaveApplication;
            })
            .then(nLeaveApplication => {
                resolve(nLeaveApplication)
            })
            .catch(errors => { reject(errors); });
    });
}

export const calculateWorkingDaysService = (startDate, endDate, weeklyOffDays) => {
    momentBusiness.locale('us', {
        workingWeekdays: weeklyWorkingDays(weeklyOffDays),
        holidays: ["01-01-2018"],
        holidayFormat: 'MM-DD-YYYY'
    });
    let addDay = momentBusiness(endDate, 'MM-DD-YYYY').isBusinessDay() ? 1 : 0;
    return momentBusiness(endDate, 'MM-DD-YYYY').businessDiff(momentBusiness(startDate, 'MM-DD-YYYY')) + addDay;
}

export const getLeaveApplicationService = () => {
    return LeaveApplication.find().populate('employee').populate('leave_type').exec();
}