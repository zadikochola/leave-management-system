import EmployeeLeaveType from '../models/EmployeeLeaveType';
import LeaveType from '../models/LeaveType';
import moment from 'moment';

export const findCurrentLeaveTypeStatusService = (employeeId, leaveTypeId) => {
    return EmployeeLeaveType
                .findOne({'leave_type':leaveTypeId, 'employee': employeeId, 'expired': false})
                .exec();
}

export const findEmployeeLeaveTypeService = (employeeId) => {
    console.log('employeeId ', employeeId)
    return EmployeeLeaveType
        .find({'employee': employeeId, 'expired': false})
        .populate('leave_type')
        .exec();
}

export const findAllLeaveTypesService = () => {
    return LeaveType.find().exec();
}

export const saveNewLeaveTypeService = (leaveType) => {
    return leaveType.save();
}

export const createEmployeeLeaveTypesService = (employeeId, data, userId) => {
    var startDate = data.startDate;
    var endDate = data.endDate;
    var leaveTypes = data.leaveTypes;
    var employeeLeaveTypes = [];

     return new Promise( async (resolve, reject) => {
             for( const leaveType of leaveTypes)  {
                 var employeeLeaveType = new EmployeeLeaveType();
                 employeeLeaveType.start_date = moment(startDate).format('MM-DD-YYYY');
                 employeeLeaveType.end_date = moment(endDate).format('MM-DD-YYYY');
                 employeeLeaveType.employee = employeeId;
                 employeeLeaveType.leave_type = leaveType.type;
                 employeeLeaveType.AddedBy = userId;
                 employeeLeaveType.expired = false;
                 await getEmployeeLeaveTypeService(employeeId, leaveType.type)
                     .then(function(oldEmployeeLeaveType){
                         if (oldEmployeeLeaveType == null) {
                             employeeLeaveType.max_days_allowed = leaveType.maxDays;
                             employeeLeaveType.days_left = leaveType.maxDays;
                         } else {
                             employeeLeaveType.max_days_allowed = leaveType.maxDays;
                             employeeLeaveType.days_left = parseInt(leaveType.maxDays, 10) + parseInt(oldEmployeeLeaveType.days_left, 10);

                             oldEmployeeLeaveType.expired = true;
                             oldEmployeeLeaveType.save();
                         }
                         employeeLeaveTypes.push(employeeLeaveType);
                     })
                     .catch((errors)=>{
                         reject(errors);
                     });
             }
             EmployeeLeaveType.insertMany(employeeLeaveTypes)
                 .then(result => { resolve(findEmployeeLeaveTypeService(employeeId)); })
                 .catch(errors => { reject(errors); });
         });
}

export const getEmployeeLeaveTypeService = (employeeId, leaveTypeId) => {
    return EmployeeLeaveType.findOne({employee: employeeId, leave_type: leaveTypeId, expired: false}).exec();
}