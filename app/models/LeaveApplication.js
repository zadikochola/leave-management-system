import mongoose from 'mongoose';
import mongooseHistory from 'mongoose-history';
const Schema = mongoose.Schema;
import {calculateWorkingDaysService} from '../services/LeaveApplicationService';

var LeaveApplicationSchema = new Schema({
        start_date: {type: Date, required: true, },
        end_date: {type: Date, required: true, },
        employee: {type: Schema.Types.ObjectId, ref: 'User'},
        resolved_status: {type: Number, required: true},
        resolved_by: {type: Schema.Types.ObjectId, ref: 'User'},
        resolved_date: {type: Date},
        leave_type: {type: Schema.Types.ObjectId, ref: 'LeaveType'},
    }, {timestamps : true});

LeaveApplicationSchema.plugin(mongooseHistory);
LeaveApplicationSchema.methods.workingDays = function(){
    return calculateWorkingDaysService(this.start_date, this.end_date, this.employee.DaysOffDuty);
}
export default mongoose.model('LeaveApplication', LeaveApplicationSchema);