/**
 * Created by Yunus on 11/26/2018.
 */
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

var ResourceSchema = new Schema({
    name: String,
    friendly_name: String,
    description: String
}, { timestamps: true });

export default mongoose.model('Resource', ResourceSchema);