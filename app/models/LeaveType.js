import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

var LeaveTypeSchema = new Schema({
        name: { type: String,unique: true, required: true, },
        AddedBy: {type: Schema.Types.ObjectId, ref: 'User'}
    }, { timestamps: true });

LeaveTypeSchema.plugin(uniqueValidator, {message: '{VALUE} already exists.'});

export default mongoose.model('LeaveType', LeaveTypeSchema);