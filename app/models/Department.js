import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

var DepartmentSchema   = new Schema({
    
    Name:{ type: String,unique: true, required: true, },
    DepartmentHead: {type: Schema.Types.ObjectId, ref: 'User'},
    AddedBy: {type: Schema.Types.ObjectId, ref: 'User'}
}, {timestamps: true});
DepartmentSchema.plugin(uniqueValidator, {message: '{VALUE} is already taken.'});

export default mongoose.model('Department', DepartmentSchema);
