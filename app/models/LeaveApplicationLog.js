import mongoose from 'mongoose';
const Schema = mongoose.Schema;

var LeaveApplicationLogSchema = new Schema({
    employee: {type: Schema.Types.ObjectId, ref: 'User'},
    resolved_status: {type: Number, required: true},
    resolved_by: {type: Schema.Types.ObjectId, ref: 'User'},
    resolved_date: {type: Date},
    leave_application: {type: Schema.Types.ObjectId, ref: 'LeaveApplication'},
}, {timestamps : true});

export default mongoose.model('LeaveApplicationLog', LeaveApplicationLogSchema);
