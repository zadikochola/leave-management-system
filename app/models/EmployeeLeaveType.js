import mongoose from 'mongoose';
const Schema = mongoose.Schema;

var EmployeeLeaveTypeSchema = new Schema({
        start_date: {type: Date, required: true, },
        end_date: {type: Date, required: true, },
        employee: {type: Schema.Types.ObjectId, ref: 'User'},
        leave_type: {type: Schema.Types.ObjectId, ref: 'LeaveType'},
        max_days_allowed: Number,
        days_left: Number,
        expired: Boolean,
        AddedBy: {type: Schema.Types.ObjectId, ref: 'User'}
    }, {timestamps: true});

export default mongoose.model('EmployeeLeaveType', EmployeeLeaveTypeSchema);