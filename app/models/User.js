import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt-nodejs';
import dateFormat from 'dateformat';
import config from '../../config/config-secret';

const Schema = mongoose.Schema;

var UserSchema = new Schema({
    Fname: String,
    Lname: String,
    Email: { type: String, trim: true, unique: true, required: true },
    Password: { type: String, trim: true, required: true },
    Phone: { type: String, required: true, unique: true },
    image: String,
    StaffNo: Number,
    UserLevel: Number,
    Supervisor: { type: Schema.Types.ObjectId, ref: 'User' },
    DaysOffDuty: { type: Array, "default": [0, 6] },
    ContractStarts: { type: Date, default: dateFormat(Date.now(), "yyyy-mm-dd") },
    ContractEnds: { type: Date, default: dateFormat(Date.now(), "yyyy-mm-dd") },
    AccountStatus: { type: Number, default: 0 },
    isHR: { type: Boolean, default: false },
    Department: { type: Schema.Types.ObjectId, ref: 'Department' },
    role: { type: Schema.Types.ObjectId, ref: 'Role' },
}, { timestamps: true });

UserSchema.plugin(uniqueValidator, { message: '{VALUE} is already taken.' });

UserSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

UserSchema.methods.validPassword = function (password) {
    console.log("password compare");
    return bcrypt.compareSync(password, this.Password);
};

UserSchema.methods.generateJWT = function () {
    var today = new Date();
    var exp = new Date(today);
    exp.setDate(today.getDate() + 60);

    return jwt.sign({
        id: this._id,
        username: this.username,
        exp: parseInt(exp.getTime() / 1000),
    }, config.secret);
};


export default mongoose.model('User', UserSchema);
