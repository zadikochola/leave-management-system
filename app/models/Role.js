/**
 * Created by Yunus on 11/26/2018.
 */
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

var RoleSchema = new Schema({
    roleName: String,
    resources : [{ type: Schema.Types.ObjectId, ref: 'Resource' }],
    parent : { type: Schema.Types.ObjectId, ref: 'Role' }
},{ timestamps: true });

export default mongoose.model('Role', RoleSchema);