"use strict";
function BadRequestError(code, error) {
    Error.call(this, error);
    Error.captureStackTrace(this, this.constructor);
    this.name = "BadRequestError";
    this.message = error;
    this.code = code;
    this.status = 400;
    this.inner = error;
}

BadRequestError.prototype = Object.create(Error.prototype);
BadRequestError.prototype.constructor = BadRequestError;

module.exports = BadRequestError;
