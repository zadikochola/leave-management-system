import express from "express";
import path from 'path';
import bodyParser from 'body-parser';
import expressValidator from "express-validator";
import cors from 'cors';
import mongoose from 'mongoose';
import morgan from 'morgan';
import methodOverride from 'method-override';
import cluster from "cluster";
import dotenv from 'dotenv';
import passport from 'passport';
import passportauth from "./config/passport";
import NotFoundError from './app/errors/NotFoundError';
import swagger from './swagger';
var isProduction = process.env.NODE_ENV === 'production';

// Create global app object
var app = express();

dotenv.config()

app.use(expressValidator());
app.use(cors());

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
      // look in urlencoded POST bodies and delete it
      var method = req.body._method
      delete req.body._method
      return method
    }
  }));


//view engine setup
app.use(express.static(path.join(__dirname, '/public')));
const options = {
    useNewUrlParser: true,
    autoIndex: false,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 500,
    poolSize: 10,
    bufferMaxEntries: 0,
    connectTimeoutMS: 10000,
    socketTimeoutMS: 45000,
    family: 4
};
mongoose.connect(process.env.MONGODB_URI,options);

// mongoose.connect(process.env.MONGODB_URI, function (error) {
//     if (error) console.error("mongo db connection error",error);
//     else console.log('mongo connected');
//  });
mongoose.connect('mongodb://localhost/iprocure_hrm');
mongoose.connection.on('connected', function(test) {
    require('./authorization').init();
});

passportauth();
app.use(passport.initialize());

console.log("test1 1")
// app.use('api/docs', require('./swagger')(app));

swagger(app);
console.log("test1 2")
app.use(require('./app/routes'));

app.use(function (req, res, next) {
    console.log(res.status())
    next(new NotFoundError(res.statusCode, {message: 'Page not found'}));
});

//Centralized Error handling middleware
app.use(function (err, req, res, next) {
    //Formulate an error response here
    var error = {};
    error.name  = err.name;
    error.message  = err.message;
    error.code  = err.code;
    error.status  = err.status || 500;

    return res.status(error.status).json(error);
});

if( cluster.isMaster ) {
    // const WORKERS = process.env.WEB_CONCURRENCY || 1;
    const WORKERS = require("os").cpus().length;
    for (let i = 0; i < WORKERS; i++) {
      cluster.fork();
    }

  cluster.on( 'online', function( worker ) {
    console.log( 'Worker ' + worker.process.pid + ' is online.' );
  });
  cluster.on( 'exit', function( worker, code, signal ) {
    cluster.fork();
    console.log( 'worker ' + worker.process.pid + ' died.' );
  });
}else{
var server = app.listen(process.env.PORT || 3001, function () {
    console.log('Listening on port ' + server.address().port);
});
}

module.exports = app;
